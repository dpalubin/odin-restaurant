import './style.css';
import gumbo from '../images/foodGumboStew.png';
import spaghetti from '../images/foodSpaghetti.png';
import tunaToast from '../images/foodTunaFishGravyToast.png';
import steak from '../images/foodSteakAndPotato.png';
import chowder from '../images/foodShamChowder.png';

function createMenuItem(title, description, imageLink, value) {
  const div = document.createElement('div');
  div.classList.add('menu-item');
  const heading = document.createElement('h4');
  heading.textContent = title;
  const p = document.createElement('p');
  p.textContent = description;
  const img = document.createElement('img');
  img.src = imageLink;
  const price = document.createElement('p');
  price.textContent = value + ' dukes';

  div.appendChild(heading);
  div.appendChild(p);
  div.appendChild(img);
  div.appendChild(price);

  return div;
}

function createMenuPage() {
  const contentDiv = document.querySelector('#content');
  
  contentDiv.appendChild(createMenuItem('Gumbo Stew',
    'Hearty, delicious stew made in classic New Orleans style',
    gumbo,
    800));
  contentDiv.appendChild(createMenuItem('Spaghetti',
    'Just like Grandma used to make',
    spaghetti,
    700));
  contentDiv.appendChild(createMenuItem('Tuna Fish Gravy Toast',
    'Seared Mahi tuna in a thick rich sauce over toasted whole grain bread',
    tunaToast,
    650));
  contentDiv.appendChild(createMenuItem('Steak and Potato Meal',
    'Cooked to order. Hearty and filling',
    steak,
    500));
  contentDiv.appendChild(createMenuItem('Sham Chowder',
    'Delicious New England style chowder with lobster and shrimp',
    chowder,
    650));
}

export default createMenuPage;