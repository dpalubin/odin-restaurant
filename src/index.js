import makeHome from './home';
import makeMenu from './menu';
import makeContact from './contact';

const homeButton = document.querySelector('.home-button');
const menuButton = document.querySelector('.menu-button');
const contactButton = document.querySelector('.contact-button');
const contentDiv = document.querySelector('#content');

function clearPage(){
  while(contentDiv.firstChild) contentDiv.removeChild(contentDiv.lastChild);
  const nav = document.querySelector('nav');
  for(const child of nav.children){
    child.classList.remove('active');
  }
}

function activateHomePage(){
  clearPage();
  homeButton.classList.add('active');
  makeHome();
}

homeButton.addEventListener('click', activateHomePage);
menuButton.addEventListener('click', (e) => {
  clearPage();
  menuButton.classList.add('active');
  makeMenu();
});
contactButton.addEventListener('click', (e) => {
  clearPage();
  contactButton.classList.add('active');
  makeContact();
});

//Initialize the website with the home page
activateHomePage();