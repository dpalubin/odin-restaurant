function createHomePage(){
  const contentDiv = document.querySelector('#content');
  
  const heading = document.createElement('h2');
  heading.textContent = 'Welcome Survivor!';
  const about = document.createElement('p');
  about.textContent = '7 Days to Dine is a restaurant serving up all your favorites from the popular video game 7 Days to Die. \
  Enjoy your Spaghetti or Gumbo Stew while taking in our post-apocalyptic, zombie themed atmosphere.';
  //const emTag = document.createElement('em');
  const testimonial = document.createElement('p');
  testimonial.innerHTML = `<em>"I found this place while wandering downtown, and the food is great! They've got all the best entrees to 
  sate your hunger and quench your thirst. I will definitely be coming back here often."</em>`;
  //testimonial.appendChild('emTag');
  const testimonalName = document.createElement('p');
  testimonalName.textContent = '-Arlene';
  testimonalName.classList.add('attribution');
  const hoursHeading = document.createElement('h3');
  hoursHeading.textContent = 'Hours';
  const hoursList = document.createElement('ul');
  const monday = document.createElement('li');
  monday.textContent = "Monday: 12pm - 10pm";
  const tuesday = document.createElement('li');
  tuesday.textContent = "Tuesday: 12pm - 10pm";
  const wednesday = document.createElement('li');
  wednesday.textContent = "Wednesday: 12pm - 10pm";
  const thursday = document.createElement('li');
  thursday.textContent = "Thursday: 12pm - 10pm";
  const friday = document.createElement('li');
  friday.textContent = "Friday: 12pm - 10pm";
  const saturday = document.createElement('li');
  saturday.textContent = "Saturday: 12pm - 10pm";
  const sunday = document.createElement('li');
  sunday.textContent = "Sunday: 12pm - 10pm (Happy hour all day for horde night!)";
  hoursList.appendChild(monday);
  hoursList.appendChild(tuesday);
  hoursList.appendChild(wednesday);
  hoursList.appendChild(thursday);
  hoursList.appendChild(friday);
  hoursList.appendChild(saturday);
  hoursList.appendChild(sunday);
  const locationHeading = document.createElement('h3');
  locationHeading.textContent = 'Location';
  const location = document.createElement('p');
  location.textContent = '44 Maple St, Navezgane, AZ 85901';

  contentDiv.appendChild(heading);
  contentDiv.appendChild(about);
  contentDiv.appendChild(testimonial);
  contentDiv.appendChild(testimonalName);
  contentDiv.appendChild(hoursHeading);
  contentDiv.appendChild(hoursList);
  contentDiv.appendChild(locationHeading);
  contentDiv.appendChild(location);
}

export default createHomePage;
