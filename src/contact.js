import './style.css';

function createContactPage(){
  const contentDiv = document.querySelector('#content');

  const heading = document.createElement('h2');
  heading.textContent = 'Contact Us';
  const reservations = document.createElement('p');
  reservations.textContent = 'Reservations: 555-1234';
  const delivery = document.createElement('p');
  delivery.textContent = 'Delivery: 555-5678';
  
  const form = document.createElement('form');
  const formHeading = document.createElement('h3');
  formHeading.textContent = 'Let us know what you think!';
  form.appendChild(formHeading);
  const nameDiv = document.createElement('div');
  const nameLabel = document.createElement('label');
  nameLabel.textContent = 'Name';
  nameLabel.setAttribute('for','name');
  nameLabel.setAttribute('aria-required','true');
  nameDiv.appendChild(nameLabel);
  const nameInput = document.createElement('input');
  nameInput.setAttribute('type','text');
  nameInput.setAttribute('id','name');
  nameInput.setAttribute('name','name');
  nameInput.setAttribute('required','true');
  nameDiv.appendChild(nameInput);
  form.appendChild(nameDiv);
  const emailDiv = document.createElement('div');
  const emailLabel = document.createElement('label');
  emailLabel.textContent = 'Email';
  emailLabel.setAttribute('for','email');
  emailLabel.setAttribute('aria-required','true');
  emailDiv.appendChild(emailLabel);
  const emailInput = document.createElement('input');
  emailInput.setAttribute('type','email');
  emailInput.setAttribute('id','email');
  emailInput.setAttribute('name','email');
  emailInput.setAttribute('required','true');
  emailDiv.appendChild(emailInput);
  form.appendChild(emailDiv);
  const messageDiv = document.createElement('div');
  const messageLabel = document.createElement('label');
  messageLabel.textContent = 'Message';
  messageLabel.setAttribute('for','message');
  messageLabel.setAttribute('aria-required','true');
  messageDiv.appendChild(messageLabel);
  const messageInput = document.createElement('textarea');
  messageInput.setAttribute('id','message');
  messageInput.setAttribute('name','message');
  messageInput.setAttribute('required','true');
  messageDiv.appendChild(messageInput);
  form.appendChild(messageDiv);
  const submitButton = document.createElement('button');
  submitButton.textContent = 'Submit';
  submitButton.setAttribute('type','button');
  form.appendChild(submitButton);

  const modal = document.createElement('div');
  modal.style['display'] = 'none';
  modal.classList.add('modal');
  const modalBox = document.createElement('div');
  modalBox.classList.add('modal-box');
  const modalMessage = document.createElement('h1');
  modalMessage.textContent = 'Thank you for your feedback!';
  modalBox.appendChild(modalMessage);
  modal.appendChild(modalBox);

  modal.addEventListener('click',() => {
    modal.style['display'] = 'none';
  });
  submitButton.addEventListener('click', () => {
    modal.style['display'] = 'flex';
  });

  contentDiv.appendChild(heading);
  contentDiv.appendChild(reservations);
  contentDiv.appendChild(delivery);
  contentDiv.appendChild(form);
  contentDiv.appendChild(modal);
}

export default createContactPage;