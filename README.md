# Odin Restaurant

This project was created as part of The Odin Project (TOP) curriculum. The project page can be found [here](https://www.theodinproject.com/lessons/node-path-javascript-restaurant-page).

## License
This software is provided under the MIT license, copyright 2024 David Palubin.